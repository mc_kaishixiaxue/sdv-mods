﻿namespace DaLion.Stardew.Ponds;

public class Constants
{
    // produce
    public const int ROE_INDEX_I = 812,
        SQUID_INK_INDEX_I = 814,
        SEAWEED_INDEX_I = 152,
        GREEN_ALGAE_INDEX_I = 153,
        WHITE_ALGAE_INDEX_I = 157,
        STURGEON_INDEX_I = 698,
        RADIOACTIVE_ORE_INDEX_I = 909,
        RADIOACTIVE_BAR_INDEX_I = 910;

    // legendary fish
    public const int CRIMSONFISH_INDEX_I = 159,
        ANGLER_INDEX_I = 160,
        LEGEND_INDEX_I = 163,
        MUTANT_CARP_INDEX_I = 682,
        GLACIERFISH_INDEX_I = 775,
        SON_OF_CRIMSONFISH_INDEX_I = 898,
        MS_ANGLER_INDEX_I = 899,
        LEGEND_II_INDEX_I = 900,
        RADIOACTIVE_CARP_INDEX_I = 901,
        GLACIERFISH_JR_INDEX_I = 902;
}