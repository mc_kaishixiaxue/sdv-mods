﻿namespace DaLion.Stardew.Prairie.Training;

internal enum Input
{
    Empty,
    Self,
    Abigail,
    FriendlyBullet,
    Enemy,
    EnemyBullet,
    Obstacle,
    Powerup,
    Coin,
    Gopher
}