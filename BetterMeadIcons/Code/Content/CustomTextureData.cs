using System.Collections.Generic;

namespace BetterMeadIcons.Content;

internal class CustomTextureData
{
	public List<string> Flowers { get; set; } = null;

	public string Mead { get; set; } = null;
}
